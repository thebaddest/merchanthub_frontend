const containers = {
  navbar: document.getElementById("navbar-container"),
  coursel: document.getElementById("coursel-container"),
  category: document.getElementById("category-container"),
  popular: document.getElementById("popular-container"),
  deals: document.getElementById("deals-container"),
  aboutUS: document.getElementById("aboutUS-container"),
  footer: document.getElementById("footer-container"),
  postAd: document.getElementById("postAd-container"),
  kycPage: document.getElementById("kycPage-container"),
  kycVerification: document.getElementById("kycVerification-container"),
  loginPage: document.getElementById("loginPage-container"),
  signUpPage: document.getElementById("signUpPage-container"),
  categoryPage: document.getElementById("categoryPage-container"),
  productPage: document.getElementById("product-container"),
  productDetails: document.getElementById("productDetails-container"),
  emailPhone: document.getElementById("emailPhone-container"),
  forgotPassword: document.getElementById("forgotPassword-container"),
  addingPost: document.getElementById("addingPost-container"),
  comments: document.getElementById("comments-container"),
  customers: document.getElementById("customers-container"),
  help: document.getElementById("help-container"),
  settings: document.getElementById("settings-container"),
};

// Function to fetch and insert content
function fetchContent(url, container) {
  fetch(url)
    .then((response) => response.text())
    .then((htmlContent) => {
      container.innerHTML = htmlContent;
    })
    .catch((error) => {
      console.error("Error fetching content:", error);
    });
}

// Fetch content for each section
fetchContent("./components/homepage/navbar.html", containers.navbar);
fetchContent("./components/homepage/coursel.html", containers.coursel);
fetchContent("./components/homepage/category.html", containers.category);
fetchContent("./components/homepage/popular.category.html", containers.popular);
fetchContent("./components/homepage/product.deal.html", containers.deals);
fetchContent("./components/homepage/about.merchant.html", containers.aboutUS);
fetchContent("./components/homepage/footer.html", containers.footer);
fetchContent("./components/merchant/post.ad.html", containers.postAd);
fetchContent("./components/merchant/kyc.page.html", containers.kycPage);
fetchContent(
  "./components/merchant/verification.page.html",
  containers.kycVerification
);
fetchContent("./components/merchant/comments.html", containers.comments);
fetchContent("./components/merchant/customers.html", containers.customers);
fetchContent("./components/merchant/help.html", containers.help);
fetchContent("./components/merchant/settings.html", containers.settings);
fetchContent("./components/merchant/dashboard.html", containers.dashboardPage);
fetchContent("./components/authpages/loginpage.html", containers.loginPage);
fetchContent("./components/authpages/signup.html", containers.signUpPage);
fetchContent(
  "./components/merchant/shopping-cart.html",
  containers.categoryPage
);
fetchContent("./components/merchant/product.html", containers.productPage);
fetchContent(
  "./components/merchant/productDetails.html",
  containers.productDetails
);
fetchContent(
  "./components/merchant/via.email.phone.html",
  containers.emailPhone
);
fetchContent(
  "./components/merchant/forgot.password.html",
  containers.forgotPassword
);
fetchContent("./components/merchant/addingPost.html", containers.addingPost);

// shopping cart
document.addEventListener("DOMContentLoaded", function () {
  var plusBtns = document.querySelectorAll(".plus-btn");
  var minusBtns = document.querySelectorAll(".minus-btn");

  // Add event listeners to plus buttons
  plusBtns.forEach(function (btn) {
    btn.addEventListener("click", function () {
      var row = btn.closest("tr");
      var inputField = row.querySelector(".quantity");
      var priceElement = row.querySelector(".price");
      var price = parseFloat(priceElement.dataset.price);
      var quantity = parseInt(inputField.value);
      inputField.value = quantity + 1;
      priceElement.textContent = (quantity + 1) * price;
      updateTotals();
    });
  });

  // Add event listeners to minus buttons
  minusBtns.forEach(function (btn) {
    btn.addEventListener("click", function () {
      var row = btn.closest("tr");
      var inputField = row.querySelector(".quantity");
      var priceElement = row.querySelector(".price");
      var price = parseFloat(priceElement.dataset.price);
      var quantity = parseInt(inputField.value);
      if (quantity > 1) {
        inputField.value = quantity - 1;
        priceElement.textContent = (quantity - 1) * price;
        updateTotals();
      }
    });
  });

  function updateTotals() {
    var totalQuantity = 0;
    var totalPrice = 0;
    var quantityInputs = document.querySelectorAll(".quantity");
    var priceElements = document.querySelectorAll(".price");
    quantityInputs.forEach(function (input, index) {
      var quantity = parseInt(input.value);
      totalQuantity += quantity;
      totalPrice += quantity * parseFloat(priceElements[index].dataset.price);
    });
    document.getElementById("total-quantity").textContent = totalQuantity;
    document.getElementById("total-price").textContent = totalPrice.toFixed(2);
  }
});

// add hovered class to selected list item
let list = document.querySelectorAll(".navigation li");

function activeLink() {
  list.forEach((item) => {
    item.classList.remove("hovered");
  });
  this.classList.add("hovered");
}

list.forEach((item) => item.addEventListener("mouseover", activeLink));

// Menu Toggle
let toggle = document.querySelector(".toggle");
let navigation = document.querySelector(".navigation");
let main = document.querySelector(".main");

toggle.onclick = function () {
  navigation.classList.toggle("active");
  main.classList.toggle("active");
};

//FETCHING ITEMS IMAGES FROM DATABASE
fetch("https://fakestoreapi.com/products?_fields=id,title,price,image,category")
  .then((data) => {
    return data.json();
  })
  .then((completedata) => {
    let cardsHTML = "";
    completedata.forEach((product) => {
      // Truncate image name if it's too long
      const truncatedTitle = truncateStringByWords(product.title, 5); // Change 5 to desired maximum number of words

      // Convert price from dollars to shillings
      const priceInShillings = (product.price * 1000).toLocaleString("en", {
        minimumFractionDigits: 2,
        maximumFractionDigits: 2,
      });

      cardsHTML += `
        <div class="card">
          <h1 class="title">${truncatedTitle}</h1>
          <img src="${product.image}" alt="img" class="images">
          <p class="price">UGX: ${priceInShillings}</p>
          <p class="category">${product.category}</p>
          <button class="button">Add to Cart</button>
        </div>`;
    });
    document.getElementById("cards").innerHTML = cardsHTML;
  })
  .catch((error) => {
    console.log(error);
  });

// Function to truncate a string by words
function truncateStringByWords(str, maxWords) {
  const words = str.split(" ");
  if (words.length > maxWords) {
    return words.slice(0, maxWords).join(" ") + "...";
  } else {
    return str;
  }
}

document.addEventListener("DOMContentLoaded", () => {
  fetch("https://fakestoreapi.com/products/category/jewelery")
    .then((response) => response.json())
    .then((data) => {
      const productsContainer = document.getElementById("jewelery");
      data.forEach((product) => {
        const productCard = createProductCard(product);
        productsContainer.appendChild(productCard);
      });
    })
    .catch((error) => {
      console.error("Error fetching products:", error);
    });

  function createProductCard(product) {
    const card = document.createElement("div");
    card.classList.add("product-card");

    const image = document.createElement("img");
    image.src = product.image;
    image.alt = product.title;
    card.appendChild(image);

    const title = document.createElement("h2");
    title.textContent = product.title;
    card.appendChild(title);

    const price = document.createElement("p");
    price.textContent = `Price: $${product.price}`;
    card.appendChild(price);

    const description = document.createElement("p");
    description.textContent = product.description;
    card.appendChild(description);

    return card;
  }
});
